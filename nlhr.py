#! /usr/bin/env python3

import nlhr_prng
import networkx as nx
from networkx.readwrite import json_graph

import logging, sys, math, json, progressbar

from collections import defaultdict
import numpy as np


with open('nlhr_grammar.json') as f:
    json_data=f.read()
    grammar_data=json.loads(json_data)
    g=json_graph.node_link_graph(grammar_data, directed=True, multigraph=False)

print("Nodes:",g.number_of_nodes(),"edges:",g.number_of_nodes())

DEBUG=False

def DiveInto(MeinPRNG, x):

    if DEBUG: print("Entering DiveInto with", x) 

    if g.node[x]["Blatt"]:
        if DEBUG: print("DiveInto habe blatt gefunden", x)
        if DEBUG: print(g.node[x]["Data"])
        AuswahlZahl=MeinPRNG.GetPRN(len(g.node[x]["Data"]))
        if DEBUG: print(len(g.node[x]["Data"]), AuswahlZahl)

        return [ g.node[x]["Data"][ AuswahlZahl ],
                 [ len(g.node[x]["Data"]) ]
               ]
    else:
        Nachbarn=list(g.successors(x))
        res=[ "", [ ] ]
        if g.node[Nachbarn[0]]["Platz"]==0:
            res=DiveInto(MeinPRNG, Nachbarn[MeinPRNG.GetPRN(len(Nachbarn))])
            res[1].append(len(Nachbarn))
        else:
            for x in sorted(Nachbarn, key=lambda a: g.node[a]["Platz"]):
                tmp=DiveInto(MeinPRNG, x)
                res[0]+=tmp[0]+" "
                for y in tmp[1]:
                    res[1].append(y)

        return res 


def ErzeugeWort(Hashwert, EntropyTrace=False):
    res=[ "", [] ]
    MeinPRNG=nlhr_prng.PRNG(Hashwert)

    Nachbarn=list(g.successors("Start"))
    assert len(Nachbarn)>0
    
    if DEBUG: print(Nachbarn[0] % ())

    if g.node[Nachbarn[0]]["Platz"]==0:
        if DEBUG: print("Start OK DiveInto")
        res=DiveInto(MeinPRNG, Nachbarn[MeinPRNG.GetPRN(len(Nachbarn))])
    else:
        for x in sorted(Nachbarn, key=lambda a: g.node[a]["Platz"]):
            tmp=DiveInto(MeinPRNG, x)
            res[0]+=tmp[0]+" "
            for y in tmp[1]:
                res[1].append(y)

    tmp=res[0]
    if len(tmp)>0:
        tmp=tmp.replace("  "," ")
        if tmp[-2:]=='. ':
            tmp=tmp[:-1]
            if tmp[-2:]==' .':
                tmp=tmp[:-2]+"."
        else:
            if tmp[-1]==' ': 
                tmp=tmp[:-1]+'.'    
            else:
                tmp+='.'
        tmp=tmp[0].upper()+tmp[1:]

    if EntropyTrace:
        return [ tmp, res[1] ]
    else:
        return tmp

logging.basicConfig(stream=sys.stderr, level=logging.INFO)

if len(sys.argv)>1:
    a=sys.argv[1:]
    for x in a:
        print(x, ErzeugeWort(x))
    sys.exit(0)

for x in ["0123456789ABCDEF", "1234", "012345", "0123456", "567",
          "cd54bdb835ee9c15d6b5cca4caf96359d5385bc6540eaeda6c1b1921b32455d1",
          "cd54bdb835ee9c15d6b5cca4caf96359d5385bc6540eaeda6c1b1921b32455d2",
          "cd54bdb835ee9c15d6b5cca4caf96359d5385bc6540eaeda6c1b1921b32455d3",
          ]:
    print(x, ErzeugeWort(x))

print()

DoStatistics=False

if DoStatistics:
    zaehler=defaultdict(int); MAX=10**2
    pb=progressbar.ProgressBar(max_value=MAX).start()
    for i in range(MAX):
        zaehler[ErzeugeWort("{}".format(i))]+=1
        pb.update(i+1)
    pb.finish()

    maximal=0; anzahl=0; 
    for i in zaehler:
        #print(i,"\n", zaehler[i])
        maximal=max(maximal, zaehler[i])
        anzahl+=1

    print("Anzahl={}, Maximal={}".format(anzahl, maximal),
          "max", np.max(list(zaehler.values())),
          "avg", np.average(list(zaehler.values())),
          "median", np.median(list(zaehler.values())),
          "mean", np.mean(list(zaehler.values())),
          "std", np.std(list(zaehler.values())))




for testwort in [ "cdf4b32455d"+str(i) for i in range(0,10) ]:
    (x,y)=ErzeugeWort(testwort, EntropyTrace=True)
    print(testwort, x, y)
    sum=0.0
    for i in y:
        sum+=math.log2(i)

    print("Auswahlentrophie:", sum)



