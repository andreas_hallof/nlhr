#! /usr/bin/env python3

import hashlib, hmac, math, sys

class PRNG:

    def __init__(self, InitialSeed):
        self.InitialSeed=InitialSeed
        self.RNGNextCounter=1
        self.Buffer=[]
        self.GetNextRNGBlocks()

    def Bits(self,x):
        tmp=int(x); assert tmp>=0
        res=[]
        while tmp>0:
            res.append(tmp & 1)
            if tmp>1:
                tmp=tmp>>1
            else:
                tmp=-1

        return res

    def GetNextRNGBlocks(self):
        for i in range(10):
            tmpHMAC=hmac.new(self.InitialSeed.encode("latin1"), "{0:d}".format(self.RNGNextCounter).encode("latin1") , hashlib.sha256)
            self.RNGNextCounter+=1
            for x in tmpHMAC.digest():
                for y in self.Bits(x):
                    self.Buffer.append(y)

    def GetPRN(self,ObereSchranke):
        assert ObereSchranke>0

        found=False

        while not found:
            MyPRN=0; i=0
            for x in range(math.ceil(math.log2(ObereSchranke))):
                if len(self.Buffer)==0:
                    self.GetNextRNGBlocks()
                y=self.Buffer.pop(0) 
                # sys.stdout.write("Got {} MyPRN={}\n".format(y, MyPRN))
                MyPRN+=y*(2**i)
                i+=1
            # sys.stdout.write("MyPRN ist aktuell {}\n".format(MyPRN))
            if MyPRN<ObereSchranke:
                # sys.stdout.write("Returning {}\n".format(MyPRN))
                return MyPRN

    def hi(self):
        return "hi" + "".join([ "{0} ".format(x) for x in self.Buffer ])


if __name__ == '__main__':

    TestHash="0123456789ABCDEF"
    test=PRNG(TestHash)

    # print(test.hi())

    for x in range(1000):
        print(test.GetPRN(200), end=" ")
    print()

    for x in range(10):
        print(test.GetPRN(2), end=" ")
    print()

