# README #

natural language hash value representation (nlhr)

A human has to compare a (256 bit) hash value with an hash value written on a
sheet of paper. To help him a human "understandable" text is generated (natural
language generation) that represents the hash value. A grammar is defined and
represented as a directed graph. The hash value (that has to be represented) is
used as a seed for an CSPRNG. From the start symbol the graph is traversed
recursively. If the outdegree of a node is > 1, the output of the CSPRNG
determines with successor node to choose for the traversal.  If the outdegree
of a node is zero, the node contains at least one word (e. g. "cat", "ship",
"squirrel").  The output of the CSPRNG determines which word to choose.  At the
end the algorithm outputs a natural language text, that does seem to make sense
and thus is "rememberable" for a short time (e. g. 60 seconds).  The human
shall compare the text on the sheet of paper and the text on the computer
screen.  If both texts are equal, then with high probability the two hash value
are equal.

This is a proof of concept implementation.

### What is this repository for? ###

PoC, research.

### Usage ###

Calculate the "human readable" representation of a SHA256 hash value:

    $ ./nlhr.py bea38ac532de2846116a209f65fde31ee122eb1b93439c8bcff09d2bc5725c03
    Nodes: 27 edges: 27
    bea38ac532de2846116a209f65fde31ee122eb1b93439c8bcff09d2bc5725c03 Eine Eule sieht einen Farbeimer auf dem Bild.

The seed for the pseudo random number generator can also be an arbitrary character string.

    $ ./nlhr.py hhh1231
    Nodes: 27 edges: 27
    hhh1231 Wo sieht der hungriger Pelikan eine Banane fallen ? Das weiss nur die gelangweilte Maus.

If no commandline arguments are given, some test values are calculated.

    $ ./nlhr.py
    Nodes: 27 edges: 27
    0123456789ABCDEF Der Spatz sieht den Hund.
    1234 Die gelangweilte Eule sieht das Auto am Abend vor Weihnachten.
    012345 Wohin beobachtet ein gutgelaunter Spatz das Auto ? Das weiss nur die lachende Eule.
    0123456 Ein hungriger Seemann wundert sich über eine Mohrrübe.
    567 Der gutgelaunter Fuchs hört eine Mohrrübe.
    cd54bdb835ee9c15d6b5cca4caf96359d5385bc6540eaeda6c1b1921b32455d1 Ein gutgelaunter Busfahrer hört die Schiffe auf dem Wasser.
    cd54bdb835ee9c15d6b5cca4caf96359d5385bc6540eaeda6c1b1921b32455d2 Die gruene Maus wundert sich über die Schiffe als es laengst zu spaet war.
    cd54bdb835ee9c15d6b5cca4caf96359d5385bc6540eaeda6c1b1921b32455d3 Die lachende Maus sieht eine Banane fallen.

    cdf4b32455d0 Eine gelangweilte Baeckerin hört das Auto als die Sonne schon unterging. [2, 5, 4, 2, 4, 9, 4, 2]
    Auswahlentrophie: 14.491853096329674
    cdf4b32455d1 Die muede Hutnadernfachverkaueferin wundert sich über den LKW. [2, 5, 4, 2, 4, 9, 4, 2]
    Auswahlentrophie: 14.491853096329674
    cdf4b32455d2 Ein gutgelaunter Fuchs beobachtet eine Banane fallen am Abend vor Weihnachten. [2, 5, 6, 2, 4, 9, 4, 2]
    Auswahlentrophie: 15.07681559705083
    cdf4b32455d3 Ein roter Busfahrer hört eine Mohrrübe. [2, 5, 6, 2, 4, 9, 4, 2]
    Auswahlentrophie: 15.07681559705083
    cdf4b32455d4 Wohin hört ein gutgelaunter Spatz den Leuchtturm ? Warum auch sagt ein roter Seemann. [6, 3, 2, 5, 6, 2, 9, 1, 2, 2, 5, 6, 2, 1]
    Auswahlentrophie: 22.153631194101656
    cdf4b32455d5 Wohin hört ein roter Pelikan das Auto ? Warum auch sagt der gutgelaunter Seemann. [6, 3, 2, 5, 6, 2, 9, 1, 2, 2, 5, 6, 2, 1]
    Auswahlentrophie: 22.153631194101656
    cdf4b32455d6 Warum beobachtet der gutgelaunter Spatz eine Banane fallen ? Warum auch sagt eine gelangweilte Baeckerin. [6, 3, 2, 5, 6, 2, 9, 1, 2, 2, 5, 4, 2, 1]
    Auswahlentrophie: 21.568668693380502
    cdf4b32455d7 Eine gelangweilte Eule beobachtet den Leuchtturm. [2, 5, 4, 2, 4, 9, 4, 2]
    Auswahlentrophie: 14.491853096329674
    cdf4b32455d8 Die muede Maus wundert sich über das Auto als die Sonne schon unterging. [2, 5, 4, 2, 4, 9, 4, 2]
    Auswahlentrophie: 14.491853096329674
    cdf4b32455d9 Die Hutnadernfachverkaueferin sieht die Schiffe als es laengst zu spaet war. [2, 5, 4, 2, 4, 9, 4, 2]
    Auswahlentrophie: 14.491853096329674



### How do I get set up? ###

python3 with the networkx package

If you want additional statics, the numpy package is required.


