#! /usr/bin/env python3

import networkx as nx
from networkx.readwrite import json_graph
import json, math, sys

g=nx.DiGraph()
g.add_node("Start", Blatt=False)

g.add_node("Satztyp-1",  Blatt=False, Platz=0); g.add_edge("Start", "Satztyp-1")
g.add_node("Sub", Blatt=False, Platz=1); g.add_edge("Satztyp-1", "Sub")
g.add_node("mSub", Blatt=False, Platz=0); g.add_edge("Sub", "mSub")
g.add_node("mArtikel", Blatt=True, Platz=1, Data=["ein", "der"]); g.add_edge("mSub", "mArtikel")
g.add_node("mAdj", Blatt=True, Platz=2, Data=["blauer", "roter", "hungriger", "gutgelaunter", ""]); g.add_edge("mSub", "mAdj")
Nomen_Data=["Busfahrer", "Spatz", "Tischler", "Fuchs", "Seemann", "Pelikan"]
g.add_node("mNomen", Blatt=True, Platz=3, Data=Nomen_Data); g.add_edge("mSub", "mNomen")

g.add_node("fSub", Blatt=False, Platz=0); g.add_edge("Sub", "fSub")
g.add_node("fArtikel", Blatt=True, Platz=1, Data=["eine", "die"]); g.add_edge("fSub", "fArtikel")
g.add_node("fAdj", Blatt=True, Platz=2, Data=["gruene", "muede", "lachende", "gelangweilte", ""]); g.add_edge("fSub", "fAdj")
g.add_node("fNomen", Blatt=True, Platz=3, Data=["Baeckerin", "Eule", "Hutnadernfachverkaueferin", "Maus"]); g.add_edge("fSub", "fNomen")

Praed_Data=["sieht", "beobachtet", "wundert sich über", "hört"]
g.add_node("Praed", Platz=2, Blatt=True, Data=Praed_Data); g.add_edge("Satztyp-1", "Praed")
Objekt_Data=["den Leuchtturm", "eine Banane fallen", "den Hund", "eine Mohrrübe", "einen Farbeimer", "ein Zebra", "das Auto", "die Schiffe", "den LKW" ]
g.add_node("Objekt", Platz=3, Blatt=True, Data=Objekt_Data); g.add_edge("Satztyp-1", "Objekt")
# Adjunktion ist germanistisch der falsche Begriff
g.add_node("Adjunktion", Platz=4, Blatt=False); g.add_edge("Satztyp-1", "Adjunktion")
g.add_node("Wo", Platz=0, Blatt=True, Data=["auf dem Bild", "auf dem Wasser", "im Wald", ""]); g.add_edge("Adjunktion", "Wo")
g.add_node("Wann", Platz=0, Blatt=True, Data=["als es laengst zu spaet war", "als die Sonne schon unterging", "am Abend vor Weihnachten", "" ]);
g.add_edge("Adjunktion", "Wann")

g.add_node("Satztyp-2",  Blatt=False, Platz=0); g.add_edge("Start", "Satztyp-2")
g.add_node("EroeffnendeFrage", Blatt=False, Platz=1); g.add_edge("Satztyp-2", "EroeffnendeFrage")
g.add_node("Fragewort", Blatt=True, Platz=1, Data=["Wo", "Warum", "Wohin", "Wann", "Weshalb", "Wie"]); g.add_edge("EroeffnendeFrage", "Fragewort")
g.add_node("Frageverb", Blatt=True, Platz=2, Data=["beobachtet", "hört", "sieht"]); g.add_edge("EroeffnendeFrage", "Frageverb")
g.add_node("Fragesubjekt", Blatt=False, Platz=3); g.add_edge("EroeffnendeFrage", "Fragesubjekt")
g.add_edge("Fragesubjekt", "mSub"); g.add_edge("Fragesubjekt", "fSub")
g.add_node("Frageobjekt", Blatt=True, Platz=4, Data=Objekt_Data); g.add_edge("EroeffnendeFrage", "Frageobjekt")
g.add_node("Fragezeichen", Blatt=True, Platz=5, Data=["?"]); g.add_edge("EroeffnendeFrage", "Fragezeichen")

g.add_node("FolgendeAntwort", Blatt=False, Platz=2); g.add_edge("Satztyp-2", "FolgendeAntwort")
g.add_node("A1",             Blatt=True,  Platz=1, Data=["Das weiss nur", "Warum auch sagt"]); g.add_edge("FolgendeAntwort", "A1")
g.add_node("Antwortsubjekt", Blatt=False, Platz=2); g.add_edge("FolgendeAntwort", "Antwortsubjekt");
g.add_edge("Antwortsubjekt", "mSub"); g.add_edge("Antwortsubjekt", "fSub")
g.add_node("Antwortpunkt",    Blatt=True,  Platz=3, Data=["."]); g.add_edge("FolgendeAntwort", "Antwortpunkt")


SollIchDenGraphZeichnen=True

if SollIchDenGraphZeichnen:
    import matplotlib.pyplot as plt
    layout=nx.spring_layout(g, k=1.1/math.sqrt(g.number_of_nodes()), iterations=100)
    plt.figure(figsize=(20,10))
    nx.draw(g, with_labels=True, node_size=60, font_size=20); 
    plt.savefig("Grammatik_Graph.png")
    plt.figure(figsize=None)
    plt.clf()

print("Nodes:",g.number_of_nodes(),"edges:",g.number_of_nodes())


DEBUG=True

def DiveIntoZaehlen(x):

    if DEBUG: print("Entering DiveInto with", x) 

    if g.node[x]["Blatt"]:
        if DEBUG: print("DiveInto habe Blatt gefunden", x)
        if DEBUG: print(g.node[x]["Data"])

        return len(g.node[x]["Data"])

    else:
        Nachbarn=list(g.successors(x))
        if g.node[Nachbarn[0]]["Platz"]==0:
            res=0
            for x in Nachbarn:
                res+=DiveIntoZaehlen(x)
        else:
            res=1
            if DEBUG: 
                print(Nachbarn)
                for x in Nachbarn:
                    print(x)
                    print(g.node[x])
                    print(g.node[x]["Platz"])

            for x in sorted(Nachbarn, key=lambda a: g.node[a]["Platz"]):
                res*=DiveIntoZaehlen(x)

        return res 


def ZaehleWorteDerSprache():

    Nachbarn=list(g.successors("Start"))
    assert len(Nachbarn)>0
    
    if DEBUG: print(Nachbarn[0] % ())

    if g.node[Nachbarn[0]]["Platz"]==0:
        res=0
        for x in Nachbarn:
            res+=DiveIntoZaehlen(x)
    else:
        res=1
        for x in sorted(Nachbarn, key=lambda a: g.node[a]["Platz"]):
            res*=DiveIntoZaehlen(x)

    return res

print("Anzahl der durch die Grammatik erzeugten Sprache:", ZaehleWorteDerSprache())

SollIchDieGrammatikSchreiben=True

if SollIchDieGrammatikSchreiben:
    with open('nlhr_grammar.json','wt') as f:
        data=json_graph.node_link_data(g);
        f.write(json.dumps(data, sort_keys=True, indent=4))

